# CETECO

Una tienda de comercio electrónico en línea dinámica y visualmente atractiva construida con React, Redux y Tailwind CSS. El proyecto se centra en proporcionar una experiencia de usuario óptima y un manejo eficiente de datos.

## Herramientas y Bibliotecas
- React
- Redux
- Redux Thunk
- Redux Toolkit
- Axios
- Tailwind CSS
