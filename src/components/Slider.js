import { useState } from 'react';
import { BsArrowLeftShort, BsArrowRightShort } from 'react-icons/bs';

import Slide from './Slide';

const data = [
  {
    id: 1,
    src: 'https://i.ibb.co/XszmG02/camera.jpg',
    headline: 'Mejora tu fotografía con una cámara DSLR',
    body: '¿Estás buscando llevar tus fotografías de productos de comercio electrónico al siguiente nivel? Una cámara DSLR es la elección correcta. Con calidad de imagen de nivel profesional y una variedad de funciones, una DSLR te ayudará a capturar imágenes impresionantes de alta resolución que harán que tus productos destaquen.',
    cta: 'Compra cámaras DSLR ahora',
    category: 'cámaras',
  },
  {
    id: 2,
    src: 'https://i.ibb.co/mtc8v16/tv.jpg',
    headline: 'Experimenta el futuro de la televisión con una smart TV',
    body: 'Mejora tu experiencia de entretenimiento con una smart TV. Con acceso a una amplia gama de servicios de transmisión y aplicaciones, podrás ver tus programas y películas favoritas en alta definición.',
    cta: 'Compra TVs y mejora ahora',
    category: 'TVs',
  },
  {
    id: 3,
    src: 'https://i.ibb.co/kmr5qQv/headphones.jpg',
    headline: 'Mejora tu experiencia auditiva',
    body: 'Lleva tu música, películas y más al siguiente nivel con nuestros auriculares. Nuestra selección ofrece una variedad de estilos y funciones, que incluyen tecnología de cancelación de ruido, conectividad inalámbrica y diseños cómodos para usar durante todo el día.',
    cta: 'Experimenta un sonido mejorado',
    category: 'auriculares',
  },
  {
    id: 4,
    src: 'https://i.ibb.co/JqxDhvZ/console.jpg',
    headline: 'Lleva tus juegos al siguiente nivel',
    body: 'Eleva tu experiencia de juego con nuestra selección de consolas de videojuegos. Desde los modelos más recientes hasta sistemas clásicos, tenemos una consola para cada tipo de jugador. Nuestras consolas ofrecen gráficos avanzados, velocidades de procesamiento rápidas y una variedad de juegos exclusivos para elegir.',
    cta: 'Compra consolas y juega ahora',
    category: 'consolas',
  },
  {
    id: 5,
    src: '//cdn.shopify.com/s/files/1/0274/5483/2778/collections/PG_Website_Brands_Smartwatches_f41b4054-8b21-4ce1-8531-7eb89c7e9f4f.jpg?v=1618234991',
    headline: 'Mantente conectado con relojes inteligentes',
    body: 'Mantente conectado y al tanto de tu día con nuestros relojes inteligentes. Nuestra selección ofrece una variedad de estilos y funciones, que incluyen seguimiento de actividad física, notificaciones de teléfono y asistentes de voz. Estos relojes son la combinación perfecta de funcionalidad y estilo.',
    cta: 'Conéctate con un reloj inteligente',
    category: 'relojes-inteligentes',
  },
];

const Slider = () => {
  const [currentSlide, setCurrentSlide] = useState(0);

  const prevBtn = () => {
    setCurrentSlide(
      currentSlide === 0 ? data.length - 1 : (prevSlide) => prevSlide - 1
    );
  };

  const nextBtn = () => {
    setCurrentSlide(
      currentSlide === data.length - 1 ? 0 : (nextSlide) => nextSlide + 1
    );
  };
  return (
    <div className="slider-frame relative">
      <div
        className="slider"
        style={{ transform: `translateX(-${100 * currentSlide}vw)` }}
      >
        {data.map((item) => (
          <Slide item={item} key={item.id} />
        ))}
      </div>
      <div className="slider-btns absolute bottom-5 left-0 right-0 z-[1] text-2xl flex gap-5 m-auto w-fit">
        <button
          onClick={prevBtn}
          className="prevArrow h-12 w-10 flex justify-center items-center bg-violet-900 text-violet-200 border-none hover:bg-gray-900 hover:text-gray-200 duration-300 "
        >
          {<BsArrowLeftShort />}
        </button>
        <button
          onClick={nextBtn}
          className="nextArrow h-12 w-10 flex justify-center items-center bg-violet-900 text-violet-200 border-none hover:bg-gray-900 hover:text-gray-200 duration-300 "
        >
          {<BsArrowRightShort />}
        </button>
      </div>
    </div>
  );
};

export default Slider;
