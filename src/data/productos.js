export const productos = [
  {
    id: 1,
    name: 'Blink Mini – Cámara de seguridad inteligente compacta para interiores con conexión enchufable',
    description:
      'Monitorea el interior de tu hogar de día y de noche con nuestra cámara de seguridad inteligente enchufable para interiores con calidad HD 1080P',
    price: 64.99,
    image:
      'https://res.cloudinary.com/dy28teazb/image/upload/v1668172648/React%20Shopping/Products/81-585-013-01_a04wkd.jpg',
    category: 'Cámara',
  },
  {
    id: 2,
    name: 'Cámara de Vlogging, Cámara Digital 4K para YouTube con WiFi',
    description:
      "Es perfecta para el 'happy snapper' que solo desea apuntar y disparar para obtener imágenes de buena calidad",
    price: 109.99,
    image:
      'https://res.cloudinary.com/dy28teazb/image/upload/v1668172649/React%20Shopping/Products/81pgsjFGpmL_qundpd.jpg',
    category: 'Cámara',
  },
  {
    id: 3,
    name: 'SAMSUNG 55 pulgadas Clase Crystal 4K UHD Serie AU8000 HDR',
    description:
      'Presencia millones de tonos de color a través de la poderosa tecnología Dynamic Crystal',
    price: 497.99,
    image:
      'https://res.cloudinary.com/dy28teazb/image/upload/v1668172649/React%20Shopping/Products/cl-uhd-tu7090-un50tu7090gxzs-rperspective-285965740_duusj5.png',
    category: 'TV',
  },
  {
    id: 4,
    name: 'Sony 65 pulgadas 4K Ultra HD TV Serie X80K: LED Smart Google TV',
    description:
      'El procesador 4K HDR X1 ofrece una imagen suave y clara, llena de colores vibrantes y contraste detallado',
    price: 698,
    image:
      'https://res.cloudinary.com/dy28teazb/image/upload/v1668172649/React%20Shopping/Products/287330_fhhcyx.jpg',
    category: 'TV',
  },
  {
    id: 5,
    name: 'Consola PlayStation 4 Slim de 1TB - Negra',
    description:
      'Cuando están presentes, las baterías tienen una capacidad que supera el 80% del equivalente nuevo',
    price: 479.99,
    image:
      'https://res.cloudinary.com/dy28teazb/image/upload/v1668172648/React%20Shopping/Products/0188101_sony-playstation-4-slim-1tb-gaming-console-ps4_550_obrjcw.jpg',
    category: 'Consola',
  },
  {
    id: 6,
    name: 'Xbox Series S Fortnite y Rocket League Bundle',
    description:
      'Obtén el paquete Xbox Series S Fortnite & Rocket League completamente digital que incluye el paquete Midnight Drive',
    price: 199,
    image:
      'https://res.cloudinary.com/dy28teazb/image/upload/v1668172649/React%20Shopping/Products/2f0a6466-be4f-45a1-868f-dce57c3c6469.0838a0a2ac552dd7273083559d7f3c70_d4i7zb.jpg',
    category: 'Consola',
  },
  {
    id: 7,
    name: 'JBL Tune 510BT: Auriculares inalámbricos supraaurales con sonido Purebass - Blanco',
    description:
      'Los auriculares inalámbricos Tune 510BT cuentan con el renombrado sonido JBL Pure Bass',
    price: 29.99,
    image:
      'https://res.cloudinary.com/dy28teazb/image/upload/v1668172649/React%20Shopping/Products/JBL_Tune_510BT_Headphone-04.jpg-500x500_vqlhjf.jpg',
    category: 'Auriculares',
  },
  {
    id: 8,
    name: 'Auriculares inalámbricos con Bluetooth y 36 horas de tiempo de reproducción con carga inalámbrica',
    description:
      'Los auriculares inalámbricos cuentan con una pantalla de doble alimentación que muestra la batería restante del estuche con un número en la pantalla',
    price: 38.99,
    image:
      'https://res.cloudinary.com/dy28teazb/image/upload/v1668172650/React%20Shopping/Products/9b890489-49e2-47aa-8b85-b71892cc16ae.3142c0cfab48b4acd4f628c8f8f39190_volxjj.jpg',
    category: 'Auriculares',
  },
  {
    id: 9,
    name: 'Apple Watch Series 4 (GPS, 40MM) - Carcasa de aluminio plateado',
    description:
      'Este producto tendrá una batería que supera el 80% de la capacidad en comparación con una nueva',
    price: 147,
    image:
      'https://res.cloudinary.com/dy28teazb/image/upload/v1668172650/React%20Shopping/Products/FU642_zstpwl.jpg',
    category: 'Relojes Inteligentes',
  },
  {
    id: 10,
    name: 'Reloj Inteligente Militar para Hombres al Aire Libre, Resistente al Agua, Reloj Inteligente Táctico',
    description:
      'El reloj inteligente militar EIGIIS cuenta con 10 certificaciones de grado militar y puede utilizarse en condiciones ambientales adversas',
    price: 49.99,
    image:
      'https://res.cloudinary.com/dy28teazb/image/upload/v1668172650/React%20Shopping/Products/Military-Smart-Watch-Men-Outdoor-Waterproof-Tactical-Smartwatch-Bluetooth-Dail-Calls-Speaker-Fitness-Tracker-for-iPhone.jpg_640x640_jy4v6s.jpg',
    category: 'Relojes Inteligentes',
  },
  {
    id: 11,
    name: 'Auriculares Airpods Max',
    description:
      'Presentamos los AirPods Max: un equilibrio perfecto entre audio de alta fidelidad emocionante y la magia sin esfuerzo de los AirPods. La experiencia de escucha personal definitiva está aquí',
    price: 1400,
    image: 'https://m.media-amazon.com/images/I/61WL8oHhasL._AC_SL1201_.jpg',
    category: 'Cámara',
  },
  {
    id: 12,
    name: 'Apple MacBook Air 13-inch Laptop',
    description:
      'Obtén la potente y ligera MacBook Air para todas tus necesidades de computación. Perfecta para el trabajo y el entretenimiento.',
    price: 1099.99,
    image: 'https://m.media-amazon.com/images/I/51dafnlz6wL._AC_SX679_.jpg',
    category: 'laptops',
  },
  {
    id: 13,
    name: 'Logitech MX Master 3 Ratón Inalámbrico',
    description:
      'Aumenta tu productividad con el ratón Logitech MX Master 3. Precisión y personalización a tu alcance.',
    price: 99.99,
    image:
      'https://m.media-amazon.com/images/I/51bPrpGZiQS.__AC_SX300_SY300_QL70_FMwebp_.jpg',
    category: 'mouse',
  },
  {
    id: 14,
    name: 'Corsair K95 RGB Platinum XT Teclado Mecánico para Juegos',
    description:
      'Mejora tu experiencia de juego con el teclado Corsair K95 RGB Platinum XT. Personalizable y sensible.',
    price: 169.99,
    image: 'https://m.media-amazon.com/images/I/7193Jl8PejL._AC_SX522_.jpg',
    category: 'teclado',
  },
  {
    id: 15,
    name: 'Dell XPS 15 Laptop con Pantalla 4K OLED',
    description:
      'Experimenta visuales impresionantes con la laptop Dell XPS 15 con pantalla 4K OLED. Ideal para profesionales creativos.',
    price: 1599.99,
    image:
      'https://m.media-amazon.com/images/I/719CAihgtTL.__AC_SY300_SX300_QL70_FMwebp_.jpg',
    category: 'laptops',
  },
  {
    id: 16,
    name: 'Logitech G Pro Ratón Inalámbrico para Juegos',
    description:
      'Domina a tus oponentes con el ratón inalámbrico Logitech G Pro. Ligero y de alto rendimiento.',
    price: 129.99,
    image:
      'https://m.media-amazon.com/images/I/51uy8gOG-iL.__AC_SX300_SY300_QL70_FMwebp_.jpg',
    category: 'mouse',
  },
  {
    id: 17,
    name: 'Teclado Mecánico para Juegos Razer BlackWidow Elite',
    description:
      'Desata tus habilidades de juego con el teclado Razer BlackWidow Elite. Iluminación RGB personalizable y teclas táctiles.',
    price: 139.99,
    image:
      'https://m.media-amazon.com/images/I/81oLMQUj4fL.__AC_SY300_SX300_QL70_FMwebp_.jpg',
    category: 'teclado',
  },
  {
    id: 18,
    name: 'Laptop HP Spectre x360 2 en 1 de 13.3 pulgadas',
    description:
      'La versátil laptop HP Spectre x360 2 en 1 de 13.3 pulgadas es perfecta para la productividad y el entretenimiento.',
    price: 1199.99,
    image: 'https://m.media-amazon.com/images/I/81y7Belj1VL._AC_SX466_.jpg',
    category: 'laptops',
  },
  {
    id: 19,
    name: 'Ratón Inalámbrico Logitech MX Anywhere 3',
    description:
      'Lleva la productividad a cualquier lugar con el ratón inalámbrico Logitech MX Anywhere 3. Compacto y versátil.',
    price: 79.99,
    image:
      'https://m.media-amazon.com/images/I/61hdaXgYTPL.__AC_SX300_SY300_QL70_FMwebp_.jpg',
    category: 'mouse',
  },
  {
    id: 20,
    name: 'Teclado Mecánico Ducky One 2 Mini RGB',
    description:
      'Personaliza tu experiencia de escritura y juego con el teclado mecánico Ducky One 2 Mini RGB. Diseño compacto y retroiluminación RGB.',
    price: 149.99,
    image:
      'https://m.media-amazon.com/images/I/61meeCvhRhL.__AC_SY300_SX300_QL70_FMwebp_.jpg',
    category: 'teclado',
  },
  {
    id: 21,
    name: 'Procesador de Escritorio Intel Core i9-11900K de 11ª Generación',
    description:
      'Aumenta el rendimiento de tu PC con el procesador Intel Core i9-11900K. Ideal para juegos y creación de contenido.',
    price: 529.99,
    image:
      'https://m.media-amazon.com/images/I/61V7FsOuaAL.__AC_SY300_SX300_QL70_FMwebp_.jpg',
    category: 'Componentes de PC',
  },
  {
    id: 22,
    name: 'Tarjeta Gráfica NVIDIA GeForce RTX 3080',
    description:
      'Experimenta gráficos impresionantes con la tarjeta gráfica NVIDIA GeForce RTX 3080. Perfecta para juegos y renderización 3D.',
    price: 799.99,
    image:
      'https://m.media-amazon.com/images/I/81SzNmM27EL.__AC_SY300_SX300_QL70_FMwebp_.jpg',
    category: 'Componentes de PC',
  },
  {
    id: 23,
    name: 'Memoria RAM Corsair Vengeance LPX DDR4 de 16GB',
    description:
      'Actualiza la memoria de tu PC con la memoria RAM Corsair Vengeance LPX DDR4 de 16GB. Mejora el rendimiento en multitarea y juegos.',
    price: 89.99,
    image:
      'https://m.media-amazon.com/images/I/51gLnYN1W7L.__AC_SY300_SX300_QL70_FMwebp_.jpg',
    category: 'Componentes de PC',
  },
  {
    id: 24,
    name: 'Unidad de Estado Sólido Samsung 1TB NVMe M.2',
    description:
      'Potencia tu PC con la unidad de estado sólido Samsung 1TB NVMe M.2. Almacenamiento ultrarrápido para tiempos de arranque y carga de aplicaciones más rápidos.',
    price: 149.99,
    image:
      'https://m.media-amazon.com/images/I/71Sr1zjPhwL.__AC_SX300_SY300_QL70_FMwebp_.jpg',
    category: 'Componentes de PC',
  },
  {
    id: 25,
    name: 'Placa Base ATX MSI B450 TOMAHAWK MAX',
    description:
      'Construye una PC potente con la placa base ATX MSI B450 TOMAHAWK MAX. Compatible con los procesadores más recientes y funciones de juego.',
    price: 129.99,
    image:
      'https://m.media-amazon.com/images/I/910jyKG9QlL.__AC_SY300_SX300_QL70_FMwebp_.jpg',
    category: 'Componentes de PC',
  },
  {
    id: 26,
    name: 'Refrigerador de CPU Cooler Master Hyper 212 RGB',
    description:
      'Mantén la CPU de tu PC fresca con el refrigerador de CPU Cooler Master Hyper 212 RGB. Refrigeración silenciosa y eficiente para tu PC.',
    price: 49.99,
    image:
      'https://m.media-amazon.com/images/I/71e7we8GyEL.__AC_SX300_SY300_QL70_FMwebp_.jpg',
    category: 'Componentes de PC',
  },
  {
    id: 27,
    name: 'Kit de Memoria RAM Crucial Ballistix DDR4 de 32GB',
    description:
      'Aumenta el rendimiento de tu PC con el kit de memoria RAM Crucial Ballistix DDR4 de 32GB. Perfecto para jugadores y creadores de contenido.',
    price: 149.99,
    image:
      'https://m.media-amazon.com/images/I/41Un7nOsFyL.__AC_SY300_SX300_QL70_FMwebp_.jpg',
    category: 'Componentes de PC',
  },
  {
    id: 28,
    name: 'Disco Duro Interno Seagate Barracuda de 2TB',
    description:
      'Amplía el almacenamiento de tu PC con el disco duro interno Seagate Barracuda de 2TB. Ideal para guardar juegos y medios.',
    price: 79.99,
    image:
      'https://m.media-amazon.com/images/I/71W881GN6-L.__AC_SX300_SY300_QL70_FMwebp_.jpg',
    category: 'Componentes de PC',
  },
  {
    id: 29,
    name: 'Placa Base ATX ASUS TUF Gaming X570-Plus',
    description:
      'Construye una PC de alto rendimiento para juegos con la placa base ATX ASUS TUF Gaming X570-Plus. Compatible con procesadores AMD Ryzen.',
    price: 199.99,
    image:
      'https://m.media-amazon.com/images/I/710hyHWebnL.__AC_SX300_SY300_QL70_FMwebp_.jpg',
    category: 'Componentes de PC',
  },
  {
    id: 30,
    name: 'Caja Mediana ATX NZXT H510 Compacta',
    description:
      'Aloja los componentes de tu PC en la caja mediana ATX NZXT H510 Compacta. Diseño elegante con excelente gestión de cables.',
    price: 79.99,
    image:
      'https://m.media-amazon.com/images/I/71HkJE0HxqL.__AC_SX300_SY300_QL70_FMwebp_.jpg',
    category: 'Componentes de PC',
  },
];
